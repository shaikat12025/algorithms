//-----------------------------------------------
// Mark: what is fizzbuzz problem?

// For this challenge you need to write a computer program that will display all the numbers between 1 and 100.
//For each number divisible by three the computer will display the word “fizz”,
//For each number divisible by five the computer will display the word “buzz”,
//For each number divisible by three and by five the computer will display the word “fizz-buzz”,
//-----------------------------------------------


import UIKit

//var number: Int = 0
//
//for number in 1...100 {
//    switch true {
//    case (number % 3 == 0) && (number % 5 == 0):
//        print("\(number) fizz_buzz")
//    case (number % 3 == 0):
//        print("\(number) fizz")
//    case (number % 5 == 0):
//        print("\(number) buzz")
//    default:
//        print(number)
//    }
//}





//-----------------------------------------------
// Mark: another approach
//-----------------------------------------------
var secondNumber : Int = 0

for secondNumber in 1...100{
    if (secondNumber % 3 == 0 && secondNumber % 5 == 0) {1234
        print("\(secondNumber) fizz_buzz")
    }
    else if (secondNumber % 3 == 0) {
        print("\(secondNumber) fizz")
    }
    
    else if (secondNumber % 5 == 0){
        print("\(secondNumber) buzz")
    }
    else {
        print(secondNumber)
    }
}

let start = CFAbsoluteTimeGetCurrent()
// run your work
let diff = CFAbsoluteTimeGetCurrent() - start
print("Took \(diff) seconds")

